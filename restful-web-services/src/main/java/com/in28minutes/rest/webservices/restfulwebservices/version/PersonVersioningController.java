package com.in28minutes.rest.webservices.restfulwebservices.version;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersioningController {

    @GetMapping(path = "v1/person")
    public PersonV1 personV1() {
        return new PersonV1("Bob Charlie");
    }

    @GetMapping(path = "v2/person")
    public PersonV2 personV2() {
        return new PersonV2(new Name("Bob", "Charlie"));
    }

    @GetMapping(path = "/person/param", params="version=1")
    public PersonV1 param1() {
        return new PersonV1("Bob Charlie");
    }

    @GetMapping(path = "/person/param", params="version=2")
    public PersonV2 param2() {
        return new PersonV2(new Name("Bob", "Charlie"));
    }

    @GetMapping(path = "/person/header", headers="X-API-VERSION=1")
    public PersonV1 header1() {
        return new PersonV1("Bob Charlie");
    }

    @GetMapping(path = "/person/header", headers="X-API-VERSION=2")
    public PersonV2 header2() {
        return new PersonV2(new Name("Bob", "Charlie"));
    }

    @GetMapping(path = "/person/produces", produces="application/vnd.company.app-v1+json")
    public PersonV1 produces1() {
        return new PersonV1("Bob Charlie");
    }

    @GetMapping(path = "/person/produces", produces="application/vnd.company.app-v2+json")
    public PersonV2 produces2() {
        return new PersonV2(new Name("Bob", "Charlie"));
    }
}
