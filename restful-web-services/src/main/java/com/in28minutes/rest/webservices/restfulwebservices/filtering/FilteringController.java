package com.in28minutes.rest.webservices.restfulwebservices.filtering;

import com.fasterxml.jackson.databind.ser.BeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class FilteringController {

    @GetMapping(path = "/filtering")
    public SomeBeanStaticFiltering retrieveSomeBean() {
        return new SomeBeanStaticFiltering("value1", "value2", "value3", "value4");
    }

    @GetMapping(path = "/filtering-list")
    public List<SomeBeanStaticFiltering> retrieveListOfSomeBeans() {
        return Arrays.asList(new SomeBeanStaticFiltering("value11", "value12", "value13", "value14"), new SomeBeanStaticFiltering("value21", "value22", "value23", "value24"));
    }

    @GetMapping(path = "/filtering-list-dynamic")
    public MappingJacksonValue retrieveListOfSomeBeansDynamic() {
        SomeBeanDynamicFiltering someBean1 = new SomeBeanDynamicFiltering("value11", "value12", "value13", "value14");
        SomeBeanDynamicFiltering someBean2 = new SomeBeanDynamicFiltering("value21", "value22", "value23", "value24");

        MappingJacksonValue mapping = new MappingJacksonValue(Arrays.asList(someBean1, someBean2));
        mapping.setFilters(createFilterProvider("SomeBeanDynamicFilter", "value2", "value3"));

        return mapping;
    }

    @GetMapping(path = "/filtering-dynamic")
    public MappingJacksonValue retrieveSomeBeanDynamic() {
        SomeBeanDynamicFiltering someBean = new SomeBeanDynamicFiltering("value11", "value12", "value13", "value14");

        MappingJacksonValue mapping = new MappingJacksonValue(someBean);
        mapping.setFilters(createFilterProvider("SomeBeanDynamicFilter", "value1", "value2"));

        return mapping;
    }

    private FilterProvider createFilterProvider(String filterName, String ... fields) {
        return new SimpleFilterProvider().addFilter(filterName, createFilter(fields));
    }

    private SimpleBeanPropertyFilter createFilter(String[] fields) {
        return SimpleBeanPropertyFilter.filterOutAllExcept(fields);
    }

}
