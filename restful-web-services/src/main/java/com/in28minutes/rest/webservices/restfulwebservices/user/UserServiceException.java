package com.in28minutes.rest.webservices.restfulwebservices.user;

public class UserServiceException extends RuntimeException {

    public UserServiceException(String message) {
        super(message);
    }
}
