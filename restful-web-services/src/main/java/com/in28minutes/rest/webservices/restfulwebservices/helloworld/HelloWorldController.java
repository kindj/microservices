package com.in28minutes.rest.webservices.restfulwebservices.helloworld;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
public class HelloWorldController {

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.GET, path="/hello-world")
    public String helloWorld() {
        return "Hello World";
    }

    @GetMapping(path="/hello-world-bean")
    public HelloWorldBean helloWorldBean() {
        return new HelloWorldBean("Hello World");
    }

    @GetMapping(path="/hello-world/path-variable/{name}")
    public HelloWorldBean helloWorldPathVariable(@PathVariable String name) {
        return new HelloWorldBean("Hello World, " + name);
    }

    @GetMapping(path="/hello-world-internationalized")
    public HelloWorldBean helloWorldInternationalized(@RequestHeader(name="Accept-Language", required = false) Locale locale) {

        //en = Hello World
        //de = Hallo Welt
        //fr = Bonjour

        return new HelloWorldBean(messageSource.getMessage("hello.world.message", null, locale));
    }

    @GetMapping(path="/hello-world-internationalized-with-localecontextholder")
    public HelloWorldBean helloWorldInternationalizedWithLocaleContextHolder() {

        //en = Hello World
        //de = Hallo Welt
        //fr = Bonjour

        return new HelloWorldBean(messageSource.getMessage("hello.world.message", null, LocaleContextHolder.getLocale()));
    }

}
