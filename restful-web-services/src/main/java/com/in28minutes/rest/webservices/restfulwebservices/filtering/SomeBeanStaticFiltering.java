package com.in28minutes.rest.webservices.restfulwebservices.filtering;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = {"value1", "value4"})
public class SomeBeanStaticFiltering {

    private final String value1;
    private final String value2;

    // Let's say this field is very secure and we don't want that field be part of the response
    @JsonIgnore
    private final String value3;

    private final String value4;

    public SomeBeanStaticFiltering(String value1, String value2, String value3, String value4) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    public String getValue1() {
        return value1;
    }

    public String getValue2() {
        return value2;
    }

    public String getValue3() {
        return value3;
    }

    public String getValue4() {
        return value4;
    }
}
