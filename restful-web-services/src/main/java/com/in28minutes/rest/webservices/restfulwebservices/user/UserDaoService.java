package com.in28minutes.rest.webservices.restfulwebservices.user;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserDaoService {

    private static List<User> users = new ArrayList<>();
    private int userCount = 3;
    private int postCount = 1;

    static {
        users.add(new User(1, "Adam", LocalDate.now()));
        users.add(new User(2, "Eve", LocalDate.now()));
        users.add(new User(3 , "Jack", LocalDate.now()));
    }

    public List<User> findAll() {
        return users;
    }

    public User save(User user) throws UserServiceException{
        if(user.getId() == null) {
            user.setId(++userCount);
        }
        users.add(user);
        return user;
    }

    public User findOne(int id) throws UserServiceException{
        Optional<User> user = users.stream().parallel().filter(u -> u.getId() == id).findAny();
        if(user.isPresent()) {
            return user.get();
        }
        throw new UserNotFoundException("User with id " + id + " does not exist");
    }

    public User deleteById(int id) {
        Optional<User> user = users.stream().filter(u -> u.getId() == id).findFirst();
        if(user.isPresent()) {
            users.removeIf(u -> u.getId().equals(id));
            return user.get();
        }
        throw new UserNotFoundException("User with id " + id + " does not exist");
    }
}
