package com.in28minutes.microservices.currencyexchangeservice;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CircuitBreakerController {

    private Logger logger = LoggerFactory.getLogger(CircuitBreakerController.class);


    @GetMapping(path = "/sample-api")
    @Retry(name = "sample-api-config", fallbackMethod = "hardcodedResponse") // Ha van akarmilyen exception a metodusban akkor a beallitott konfiguraciotol fuggoen ("default") annyiszor meghivja a metodust. Ha minden probalkozas sikertelen akkor megy vissza
    //Mi van a tranzakciokkal?
    public String sampleAPI() {
        logger.info("sampleAPI call received");
        ResponseEntity<String> forEntity = new RestTemplate().getForEntity("http://localhost:8080/some-dummy-url", String.class);
        return forEntity.getBody() ;
    }

    @GetMapping(path = "/sample-api-circuit")
    @CircuitBreaker(name = "default", fallbackMethod = "hardcodedResponseCircuit")
    public String sampleAPICircuit() {
        logger.info("sampleAPI call received");
        ResponseEntity<String> forEntity = new RestTemplate().getForEntity("http://localhost:8080/some-dummy-url", String.class);
        return forEntity.getBody() ;
    }

    @RateLimiter(name = "default")
    @GetMapping(path = "/sample-api-rl")
    public String sampleAPIRateLimiter() {
        logger.info("Sample api Rate limiting call received");
        return "Rate limiting";
    }

    @GetMapping(path = "/sample-api-bulkhead")
    @Bulkhead(name = "default")
    public String sampleAPIBulkhead() throws InterruptedException {
        logger.info("Sample api Rate limiting call received");
        new Thread().run();
        return "Bulkhead";
    }


    public String hardcodedResponse(Exception exception) {
        return "Fallback response";
    }

    public String hardcodedResponseCircuit(Exception exception) {
        return "Fallback response circuit";
    }

}
